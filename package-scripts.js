const npsUtils = require("nps-utils");
module.exports = {
   scripts: {
      ng: "ng",
      default: "nps help",
      build: "ng build",
      test: "ng test --watch --progress=false",
      lint: "ng lint",
      e2E: "ng e2e",
      prod: "ng build --prod",
      serve: "ng serve --open --port=4400",
      dev: npsUtils.concurrent.nps("serve", "test"),
      push: "nps patch gitpush",
      patch: "npm version patch",
      gitpush: "git push",
   },
};
