// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { version } from '../../package.json';

export const environment = {
   production: false,
   appVersion: version,
   firebaseConfig: {
      apiKey: 'AIzaSyB4YE4fsOP912rY-BxulQ3--RkW9Ex1DVo',
      authDomain: 'leatherjackettech.firebaseapp.com',
      databaseURL: 'https://leatherjackettech.firebaseio.com',
      projectId: 'leatherjackettech',
      storageBucket: 'leatherjackettech.appspot.com',
      messagingSenderId: '301952135710',
      appId: '1:301952135710:web:8b0e359d67505c341b740f',
      measurementId: 'G-CC27V5Q7VR',
   },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
