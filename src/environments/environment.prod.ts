import { version } from '../../package.json';

export const environment = {
   production: true,
   appVersion: version,
   firebaseConfig: {
      apiKey: 'AIzaSyB4YE4fsOP912rY-BxulQ3--RkW9Ex1DVo',
      authDomain: 'leatherjackettech.firebaseapp.com',
      databaseURL: 'https://leatherjackettech.firebaseio.com',
      projectId: 'leatherjackettech',
      storageBucket: 'leatherjackettech.appspot.com',
      messagingSenderId: '301952135710',
      appId: '1:301952135710:web:8b0e359d67505c341b740f',
      measurementId: 'G-CC27V5Q7VR',
   },
};
