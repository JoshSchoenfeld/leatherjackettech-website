import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { AppService } from './app.service';

@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
   constructor(private titleService: Title, private appService: AppService) {}
   title = 'Leather Jacket Tech';

   valueFromFirestore;
   numberFromFirestore;
   ngOnInit() {
      console.log('LJT Website, Version', environment.appVersion);

      this.titleService.setTitle(this.title);

      // this.appService.getValueFromFirestore().subscribe((response) => {
      //    this.valueFromFirestore = response.data().myFirstStringField;
      // });

      this.appService.snapshotValueFromFirestore().subscribe((response) => {
         this.valueFromFirestore = response.payload.data()[
            'myFirstStringField'
         ];
         this.numberFromFirestore = response.payload.data()[
            'my-first-number-field'
         ];
         console.log(
            this.numberFromFirestore +
               ' | ' +
               this.valueFromFirestore +
               ' : From Firestore',
         );
      });
   }
}
