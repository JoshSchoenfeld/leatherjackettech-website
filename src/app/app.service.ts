import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
   providedIn: 'root',
})
export class AppService {
   constructor(private firestore: AngularFirestore) {}
   getValueFromFirestore() {
      return this.firestore
         .collection('my-first-collection')
         .doc('d7GCeiXK1EIzg77BKWp2')
         .get();
   }
   snapshotValueFromFirestore() {
      return this.firestore
         .collection('my-first-collection')
         .doc('d7GCeiXK1EIzg77BKWp2')
         .snapshotChanges();
   }
}
