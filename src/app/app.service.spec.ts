import { TestBed } from '@angular/core/testing';

import { AppService } from './app.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';

describe('AppService', () => {
   let service: AppService;

   beforeEach(() => {
      TestBed.configureTestingModule({
         imports: [
            AngularFireModule.initializeApp(environment.firebaseConfig),
            AngularFirestoreModule,
         ],
      });
      service = TestBed.inject(AppService);
   });

   it('should be created', () => {
      expect(service).toBeTruthy();
   });
});
